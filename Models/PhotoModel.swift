import Foundation

class PhotoModel: NSObject, Codable {
    var photoURL: URL?
    var isLiked: Bool = false
    var comment: String?
    var name: String?
    
    init(photoURL: URL, isLiked: Bool, comment: String, name: String?) {
        self.photoURL = photoURL
        self.isLiked = isLiked
        self.comment = comment
        self.name = name
    }
    
    public enum CodingKeys: String, CodingKey {
        case photoURL, isLiked, comment, name
    }
    
    public override init() {}
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.photoURL, forKey: .photoURL)
        try container.encode(self.isLiked, forKey: .isLiked)
        try container.encode(self.comment,  forKey: .comment)
        try container.encode(self.name,  forKey: .name)
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.photoURL = try container.decodeIfPresent(URL.self, forKey: .photoURL)
        self.isLiked = try container.decodeIfPresent(Bool.self, forKey: .isLiked) ?? false
        self.comment = try container.decodeIfPresent(String.self, forKey: .comment)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
    }
}
