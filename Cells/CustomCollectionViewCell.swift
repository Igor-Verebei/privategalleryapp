import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    func setImage(with image: UIImage?) {
        imageView.image =  image
    }
}
