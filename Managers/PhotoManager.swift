import Foundation
import UIKit

class PhotoManager {
    static var shared = PhotoManager()
    private init(){}
    private let settingsKey = "key"
    
    func getPhotos() -> [PhotoModel] {
        if let models = UserDefaults.standard.value([PhotoModel].self, forKey: settingsKey) {
            return models
        }
        return [PhotoModel]()
    }
    
    func setPhotos(_ items: [PhotoModel]) {
        UserDefaults.standard.set(encodable: items, forKey: settingsKey)
    }
    
    func loadImageFromDiskWith(fileName: String) -> UIImage? {
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image
        }
        return nil
    }
}



