import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var swipeImageView: UIImageView!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var likeButtonOutlet: UIBarButtonItem!
    private var currentImage = 0
    var currentPhoto: PhotoModel?
    var photoArray = PhotoManager.shared.getPhotos()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
        setGestures()
        registerKeyboardNotifications()
    }
    
    private func setUI() {
        self.loadPhotoAtributes()
        swipeImageView.image = PhotoManager.shared.loadImageFromDiskWith(fileName: currentPhoto?.name ?? "")
        commentLabel.text = currentPhoto?.comment
    }
    
    private func setGestures() {
        let recSwLeft = UISwipeGestureRecognizer(target: self, action: #selector(prevButton(_:)))
        recSwLeft.direction = .left
        self.view.addGestureRecognizer(recSwLeft)
        
        let recSwRight = UISwipeGestureRecognizer(target: self, action: #selector(nextButton(_:)))
        recSwRight.direction = .right
        self.view.addGestureRecognizer(recSwRight)
    }
    
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func loadPhotoAtributes() {
        PhotoManager.shared.getPhotos()
        self.swipeImageView.image = PhotoManager.shared.loadImageFromDiskWith(fileName: photoArray[currentImage].name ?? "")
        self.commentLabel.text = photoArray[currentImage].comment
    }
    
    func loadPhotoAfterDeleteButton() {
        self.swipeImageView.image = PhotoManager.shared.loadImageFromDiskWith(fileName: photoArray.last?.name ?? "")
        self.commentLabel.text = photoArray.last?.comment
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButton(_ sender: UIGestureRecognizer) {
        if currentImage == photoArray.count - 1 {
            currentImage = 0
        } else {
            currentImage += 1
        }
        self.loadPhotoAtributes()
        self.swipeImageView.center.x -= 400
        UIImageView.animate(withDuration: 0.3, animations: {
            self.swipeImageView.center.x += 400 })
    }
    
    @IBAction func prevButton(_ sender: UIGestureRecognizer) {
        if currentImage == 0 {
            currentImage = photoArray.count - 1
        } else {
            currentImage -= 1
        }
        self.loadPhotoAtributes()
        self.swipeImageView.center.x += 400
        UIImageView.animate(withDuration: 0.3, animations: {
            self.swipeImageView.center.x -= 400 })
    }
  
    @IBAction func deleteImageButton(_ sender: UIBarButtonItem) {
        let deleteAlert = UIAlertController(title: "Delete photo?".localization(), message: "", preferredStyle: .actionSheet)
        let okAction = UIAlertAction(title: "Delete Photo".localization(), style: .destructive) { [weak self] (_) in
            guard let self = self else { return }
            self.photoArray.remove(at: self.currentImage)
            PhotoManager.shared.setPhotos(self.photoArray)
            PhotoManager.shared.getPhotos()
            self.loadPhotoAfterDeleteButton()
        }
        let cancelAction = UIAlertAction(title: "Cancel".localization(), style: .cancel) { (_) in
        }
        deleteAlert.addAction(okAction)
        deleteAlert.addAction(cancelAction)
        self.present(deleteAlert, animated: true)
    }
    
    @IBAction  func keyboardWillShow(_ notification: NSNotification) {
        let userInfo = notification.userInfo!
        let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        bottomConstraint.constant = keyboardScreenEndFrame.height
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func keyboardWillHide(){
        bottomConstraint.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func likeButton(_ sender: UIBarButtonItem) {
        if photoArray[currentImage].isLiked {
            disLike()
        } else {
            like()
        }
        loadPhotoAtributes()
    }
    
    func like(){
        photoArray[currentImage].isLiked = true
        PhotoManager.shared.setPhotos(photoArray)
    }
    
    func disLike(){
        photoArray[currentImage].isLiked = false
        PhotoManager.shared.setPhotos(photoArray)
    }
    
    @IBAction func addCommentButton(_ sender: UIButton) {
        let text = commentTextField.text
        photoArray[currentImage].comment = text
        PhotoManager.shared.setPhotos(photoArray)
        self.view.endEditing(true)
        commentTextField.text = nil
        self.loadPhotoAtributes()
    }
    
    @IBAction func deleteCommentButton(_ sender: UIButton) {
        photoArray[currentImage].comment = nil
        self.loadPhotoAtributes()
    }
    @IBAction func shareButton(_ sender: UIBarButtonItem) {
        let item = PhotoManager.shared.loadImageFromDiskWith(fileName: currentPhoto?.name ?? "")
        let shareVC = UIActivityViewController(activityItems: [item], applicationActivities: nil)
        self.present(shareVC, animated: true, completion: nil)
    }
}

extension DetailViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

