import UIKit
import Crashlytics
import SwiftyKeychainKit
import LocalAuthentication

class LogInViewController: UIViewController {
    private var context = LAContext()
    private var value: String = ""
    private let passService = Keychain(service: "Ihar-Verebei.photoGallery")
    private let passKey = KeychainKey<String>(key: "myPass" )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        faceId()
        context.canEvaluatePolicy(.deviceOwnerAuthentication, error: nil)
        
        let temp = try? passService.get(passKey)
        print(temp)
        guard let tempValue = temp else {return}
        self.value = tempValue
    }
    
    @IBAction func singInButon(_ sender: UIButton) {
        singInAlert(title: "Attention".localization(), message: "Enter pass".localization())
    }
    
    @IBAction func setPassButton(_ sender: UIButton) {
        chekOldPass()
    }
    
    func setPass() {
        let setAlert = UIAlertController(title: "Attention".localization(), message: "Set new pass".localization(), preferredStyle: .alert)
        setAlert.addTextField { (setTextField) in
            setTextField.isSecureTextEntry = true
        }
        let setAction = UIAlertAction(title: "Set".localization(), style: .default) { (_) in
            self.value = setAlert.textFields?.first?.text ?? ""
            try? self.passService.set(self.value, for: self.passKey)
            self.showOkAlert(title: "", message: "Pass changed".localization()) { (_) in
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel".localization(), style: .cancel, handler: nil)
        setAlert.addAction(setAction)
        setAlert.addAction(cancelAction)
        self.present(setAlert, animated: true)
    }
    
    func chekOldPass() {
        let alertCtrl = UIAlertController(title: "Attention".localization(), message: "Enter old pass".localization(), preferredStyle: .alert)
        alertCtrl.addTextField { (textField) in
            textField.placeholder = "Old pass".localization()
            textField.isSecureTextEntry = true
        }
        
        let okAction = UIAlertAction(title: "OK", style: .default) { [weak self] _ in
            guard let self = self else { return }
            if alertCtrl.textFields?.first?.text == self.value {
                self.setPass()
            } else {
                self.showOkAlert(title: "Wrong old pass".localization(), message: "Try again".localization()) { (_) in
                    self.singInAlert(title: "Attention".localization(), message: "Set pass for entry".localization())
                }
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel".localization(), style: .cancel, handler: nil)
        alertCtrl.addAction(okAction)
        alertCtrl.addAction(cancelAction)
        self.present(alertCtrl, animated: true)
    }
    
    func addController() {
        guard let mainController = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
        self.navigationController?.pushViewController(mainController, animated: true)
    }
    
    func singInAlert(title: String?, message: String?) {
        let alertCtrl = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertCtrl.addTextField { (textField) in
            textField.placeholder = "Enter pass".localization()
            textField.isSecureTextEntry = true
        }
        
        let okAction = UIAlertAction(title: "OK", style: .default) { [weak self] (_) in
            guard let self = self else {return}
            if alertCtrl.textFields?.first?.text == self.value {
                self.addController()
            } else {
                self.showOkAlert(title: "Wrong pass".localization(), message: "Try again".localization()) { (_) in
                    self.singInAlert(title: "Attention".localization(), message: "Write pass for entry".localization())
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel".localization(), style: .cancel, handler: nil)
        alertCtrl.addAction(okAction)
        alertCtrl.addAction(cancelAction)
        self.present(alertCtrl, animated: true)
    }
    
    func faceId() {
        context = LAContext()
        var error: NSError?
        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
            let reason = "Log in to your account".localization()
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason ) { success, error in
                if success {
                    DispatchQueue.main.async { [unowned self] in
                        self.addController()
                    }
                } else {
                    print(error?.localizedDescription ?? "Failed to authenticate")
                }
            }
        } else {
            print(error?.localizedDescription ?? "Can't evaluate policy")
        }
    }
    @IBAction func faceIdButton(_ sender: UIButton) {
        faceId()
    }
}
