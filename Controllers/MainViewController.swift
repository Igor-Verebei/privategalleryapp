import UIKit

class MainViewController: UIViewController {
    
    var photoGallery = PhotoManager.shared.getPhotos()
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imagePicker.delegate = self
    }
    
    func pickCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            self.imagePicker.sourceType = .camera
            present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    func pickPhoto() {
        self.imagePicker.sourceType = .photoLibrary
        present(self.imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func addButton(_ sender: UIButton) {
        let alertCtrl = UIAlertController(title: "Add photo".localization(), message: "Select source".localization(), preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera".localization(), style: .default) { (_) in
            self.pickCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery".localization(), style: .default) { (_) in
            self.pickPhoto()
        }
        let cancelAction = UIAlertAction(title: "Cancel".localization(), style: .cancel) { (_) in
            
        }
        alertCtrl.addAction(cameraAction)
        alertCtrl.addAction(galleryAction)
        alertCtrl.addAction(cancelAction)
        self.present(alertCtrl, animated: true)
    }
    
    @IBAction func showGalleryButton(_ sender: UIButton) {
        
        if photoGallery.isEmpty {
            self.showOkAlert(title: "Attention".localization(), message: "Gallery is empty. Add photo".localization()) { (_) in
            }
        }
        
        guard let collectionController = self.storyboard?.instantiateViewController(withIdentifier: "CVController") as? CVController else {return}
        self.navigationController?.pushViewController(collectionController, animated: true)
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    func saveImage(image: UIImage, photoModel: PhotoModel){
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let name = UUID().uuidString
        let fileURL = documentsDirectory.appendingPathComponent(name)
        guard let data = image.jpegData(compressionQuality: 1) else { return }
        
        photoModel.photoURL = fileURL
        photoModel.isLiked = false
        photoModel.comment = ""
        photoModel.name = name
        
        do {
            try data.write(to: fileURL)
            print("Image Saved")
        } catch let error {
            print("error to write image", error)
        }
    }
}

extension MainViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            
            let photoItem = PhotoModel()
            self.saveImage(image: pickedImage, photoModel: photoItem)
            print(photoItem.photoURL)
            photoGallery.append(photoItem)
            PhotoManager.shared.setPhotos(photoGallery)
            print(photoGallery.count)
            picker.dismiss(animated: true, completion: nil)
            self.showOkAlert(title: "Photo added".localization(), message: "") { (_) in
            }
        }
    }
}


