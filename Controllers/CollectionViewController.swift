import UIKit

class CVController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var imageArray = PhotoManager.shared.getPhotos()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as? DetailViewController
        guard let currentItem = sender as? PhotoModel else { return }
        vc?.currentPhoto = currentItem
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension CVController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell", for: indexPath) as? CustomCollectionViewCell else { return UICollectionViewCell() }
        
        DispatchQueue.main.async {
            cell.setImage(with: PhotoManager.shared.loadImageFromDiskWith(fileName: self.imageArray[indexPath.item].name!))
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let currentItem = imageArray[indexPath.item]
        print(currentItem)
        self.performSegue(withIdentifier: "show", sender: currentItem)
    }
}

extension CVController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let myWidth = self.view.frame.width - 10
        return CGSize(width: myWidth/3, height: myWidth/3)
    }
}
