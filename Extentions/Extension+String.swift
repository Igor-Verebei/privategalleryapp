import Foundation

extension String {
    func localization() -> String {
        return NSLocalizedString(self, comment: "")
    }
}
