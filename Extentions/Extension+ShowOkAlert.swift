import UIKit

extension UIViewController {
    func showOkAlert(title: String?, message: String?, handlerCode: @ escaping ((UIAlertAction)-> Void)) {
        let alertCtrl = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: handlerCode)
        alertCtrl.addAction(okAction)
        self.present(alertCtrl, animated: true)
    }
}
